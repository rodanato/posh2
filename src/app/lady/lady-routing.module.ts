import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/lady/home/home.module#HomeModule'
  },
  // {
  //   path: 'new-in',
  //   loadChildren: 'app/lady/new-in/new-in.module#NewInModule'
  // },
  // {
  //   path: 'ropa',
  //   loadChildren: 'app/lady/ropa/ropa.module#RopaModule'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LadyRoutingModule { }
