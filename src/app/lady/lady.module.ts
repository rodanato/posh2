import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { LadyRoutingModule } from './lady-routing.module';


@NgModule({
  imports: [
    LadyRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class LadyModule { }
