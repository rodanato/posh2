import {
  Component,
  OnInit,
  ElementRef
} from '@angular/core';
import { lory, Lory } from 'lory.js';

@Component({
  selector: 'posh-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.scss']
})
export class RecommendedComponent implements OnInit {
  private mainSlider: Lory;
  private autoSlider: any;
  private autoSliderTime    = 5000;
  private sliderOptions     = {
    infinite: 1,
    enableMouseEvents: true
  };

  // private simple_dots   = document.querySelector('.js_simple_dots');
  // private dot_list_item = document.createElement('li');
  // private dot_count     = this.simple_dots.querySelectorAll('.js_slide').length;
  // private dot_container = this.simple_dots.querySelector('.js_dots');


  constructor(private el: ElementRef) { }

  ngOnInit() {
    // this.startSlider();
  }

  handleDotEvent (e) {
    // if (e.type === 'before.lory.init') {
    //   for (let i = 0, len = this.dot_count; i < len; i++) {
    //     const clone = this.dot_list_item.cloneNode();
    //     this.dot_container.appendChild(clone);
    //   }
    // }

    if (e.type === 'after.lory.slide') {
      this.stopAutoSlider();
      this.startAutoSlider();
    }
  }

  startAutoSlider () {
    this.autoSlider = setInterval(() => this.mainSlider.next(), this.autoSliderTime);
  }

  startSlider () {
    this.mainSlider = lory(this.el.nativeElement, this.sliderOptions);

    this.el.nativeElement.addEventListener('after.lory.slide', this.handleDotEvent.bind(this));
    this.startAutoSlider();
  }

  stopAutoSlider () {
    clearInterval(this.autoSlider);
  }

}
