import {
  Component,
  OnInit,
  ElementRef
} from '@angular/core';
import { lory, Lory } from 'lory.js';

@Component({
  selector: 'posh-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  private mainSlider: Lory;
  private autoSlider: any;
  private autoSliderTime    = 5000;
  private sliderInitialSize = window.innerWidth;
  private sliderOptions     = {
    infinite: 1
  };

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.startSlider();
  }

  handleDotEvent (e) {
    if (e.type === 'after.lory.slide') {
      this.stopAutoSlider();
      this.startAutoSlider();
    }
  }

  startAutoSlider () {
    this.autoSlider = setInterval(() => this.mainSlider.next(), this.autoSliderTime);
  }

  startSlider () {
    this.mainSlider = lory(this.el.nativeElement, this.sliderOptions);

    this.el.nativeElement.addEventListener('after.lory.slide', this.handleDotEvent.bind(this));
    this.startAutoSlider();
  }

  stopAutoSlider () {
    clearInterval(this.autoSlider);
  }

}
