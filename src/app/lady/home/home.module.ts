import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home/home-routing.module';
import { SliderComponent } from './slider/slider.component';
import { MainProductsComponent } from './main-products/main-products.component';
import { ShopProductsComponent } from './shop-products/shop-products.component';
import { BannerComponent } from './banner/banner.component';
import { RecommendedComponent } from './recommended/recommended.component';

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    SliderComponent,
    MainProductsComponent,
    ShopProductsComponent,
    BannerComponent,
    RecommendedComponent
  ],
  bootstrap: [HomeComponent]
})
export class HomeModule { }
