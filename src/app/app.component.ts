import { Component } from '@angular/core';

@Component({
  selector: 'posh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'posh';
}

