import { browser, by, element } from 'protractor';

export class PoshPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('posh-root h1')).getText();
  }
}
