import { PoshPage } from './app.po';

describe('posh App', () => {
  let page: PoshPage;

  beforeEach(() => {
    page = new PoshPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to posh!!');
  });
});
